Программа создает вопросы для gitlab.

В  **/Config/Config.php** настраиваются следующие параметры:
- host    адрес gitlab сервера
- ENCRYPTION_KEY  строка для шифрования токена

Выполняет **4 операции**:
- php GitlabIssues.php list   вывод списка всех вопросов
- php GitlabIssues.php create добавление
- php GitlabIssues.php update изменение
- php GitlabIssues.php delete удаление

Для **авторизации** необходимо выполнить
- php GitlabIssues.php auth
