<?php

    class Issue {
        private $host;
        private $token;

        public function __construct($host, $token=null)
		{
            $this->host = $host;
            $this->token =$token;
		}

        public function getList(){
            $path = "/api/v4/issues";
            $ch = curl_init($this->host.$path);
 
            $headers = [
                "Authorization:Bearer $this->token"
            ];
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
 
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
 
            $result = curl_exec($ch);
            curl_close($ch);
            return json_decode($result, true);
        }

        public function create($issue){
            $path = "/api/v4/projects/".$issue['id']."/issues";

            $ch = curl_init($this->host.$path);

            curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode($issue) );
            curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json', "Authorization:Bearer $this->token"));
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

            $result = curl_exec($ch);
            curl_close($ch);
            return $result;
        }

        public function update($issue){
            $path = "/api/v4/projects/".$issue['id']."/issues/".$issue['issue_iid']
                ."?title=".$issue['title']."&description=".$issue['description'];

            $ch = curl_init($this->host.$path);

            curl_setopt( $ch, CURLOPT_HTTPHEADER, array("Authorization:Bearer $this->token"));
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');

            $result = curl_exec($ch);
            curl_close($ch);
            return $result;
        }

        public function delete($issue){
            $path = "/api/v4/projects/".$issue['id']."/issues/".$issue['issue_iid'];

            $ch = curl_init($this->host.$path);

            curl_setopt( $ch, CURLOPT_HTTPHEADER, array("Authorization:Bearer $this->token"));
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');

            $result = curl_exec($ch);
            curl_close($ch);
            return $result;
        }
    }

?>