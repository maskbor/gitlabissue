<?php

    class Project {
        private $host;
        private $path = "/api/v4/projects?owned=1";
        private $token;

        public function __construct($host, $token=null)
		{
            $this->host = $host;
            $this->token =$token;
		}

        public function getList(){
            $ch = curl_init($this->host.$this->path);
 
            $headers = [
                "Authorization:Bearer $this->token"
            ];
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
 
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
 
            $result = curl_exec($ch);
            
            return json_decode($result, true);
        }
    }

?>