<?php
    class Auth{
        private $host;

        public function __construct($host)
		{
            $this->host = $host;
        }
        
        public function auth($username, $password){
            $path = "/oauth/token";

            $ch = curl_init($this->host.$path);

            curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode([
                "grant_type"=>"password",
                "username" => $username, 
                "password" => $password
            ]) );
            curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

            $result = curl_exec($ch);
            curl_close($ch);
            return json_decode($result, true);
        }
    }
?>