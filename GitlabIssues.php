<?php
    include_once './Config/Config.php';
    require_once './Models/Issue.php';
    require_once './Models/Auth.php';
    require_once './Models/Project.php';

    if(file_get_contents('auth'))
        $token = decrypt(file_get_contents('auth'), $config['ENCRYPTION_KEY']);
    else $token = '';

foreach ($argv as $arg) {
    if($arg==="auth") { auth($config) ;}
    if($arg==="list") { showListIssues($config, $token); }
    if($arg==="create") { createIssue($config, $token); }
    if($arg==="update") { updateIssue($config, $token); }
    if($arg==="delete") { deleteIssue($config, $token); }
}

function auth($config){
    $auth = new Auth($config['host']);
    $username = readline ("Введите логин: ");
    $pwd = readline ("Введите пароль: ");
    $ret = $auth->auth($username, $pwd);
    file_put_contents('auth', encrypt($ret['access_token'], $config['ENCRYPTION_KEY']));
}

function showListIssues($config, $token){
    $issue = new Issue($config['host'], $token);
    $list = $issue->getList();
    if(count($list)>0){
        foreach($list as $item){
            echo $item['iid']."  ".$item['state']."  ".$item['title']."  ".$item['description']."\n";
        }
    } else { echo "Список пуст\n"; }
}

function showListProjects($config, $token){
    $project = new Project($config['host'], $token);
    $list = $project->getList();
    if(count($list)>0){
        echo "id    Название    Описание\n";
        foreach($list as $item){
            echo $item['id']."  ".$item['name']."  ".$item['description']."\n";
        }
    } else { echo "Список пуст\n"; }
}

function createIssue($config, $token){
    $newIssue = [
        'id' => 0,
        'title' => '',
        'description' => ''
    ];
    showListProjects($config, $token);
    $newIssue['id'] = readline ("Введите id проекта: ");
    $newIssue['title'] = readline ("Введите заголовок: ");
    $newIssue['description'] = readline ("Введите описание: ");
    
    $issue = new Issue($config['host'], $token);
    echo $issue->create($newIssue);
}

function updateIssue($config, $token){
    $newIssue = [
        'id' => 0,
        'issue_iid' => 0,
        'title' => '',
        'description' => ''
    ];
    showListProjects($config, $token);
    $newIssue['id'] = readline ("Введите id проекта: ");
    showListIssues($config, $token);
    $newIssue['issue_iid'] = readline ("Введите id вопроса: ");
    $newIssue['title'] = readline ("Введите заголовок: ");
    $newIssue['description'] = readline ("Введите описание: ");
    
    $issue = new Issue($config['host'], $token);
    echo $issue->update($newIssue);
}

function deleteIssue($config, $token){
    $newIssue = [
        'id' => 0,
        'issue_iid' => 0,
    ];
    showListProjects($config, $token);
    $newIssue['id'] = readline ("Введите id проекта: ");
    showListIssues($config, $token);
    $newIssue['issue_iid'] = readline ("Введите id вопроса: ");
    
    $issue = new Issue($config['host'], $token);
    echo $issue->delete($newIssue);
}

function encrypt($text, $key){
    $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
    $iv = openssl_random_pseudo_bytes($ivlen);
    $ciphertext_raw = openssl_encrypt($text, $cipher, $key, $options=OPENSSL_RAW_DATA, $iv);
    $hmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary=true);
    $ciphertext = base64_encode( $iv.$hmac.$ciphertext_raw );
    return $ciphertext;
} 

function decrypt($text, $key){
    $c = base64_decode($text);
    $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
    $iv = substr($c, 0, $ivlen);
    $hmac = substr($c, $ivlen, $sha2len=32);
    $ciphertext_raw = substr($c, $ivlen+$sha2len);
    $plaintext = openssl_decrypt($ciphertext_raw, $cipher, $key, $options=OPENSSL_RAW_DATA, $iv);
    $calcmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary=true);
    return $plaintext;
}